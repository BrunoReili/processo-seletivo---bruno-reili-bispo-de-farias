﻿# HOWTO

## Configurações do projeto:

**PASSOS NECESSÁRIOS PARA RODAR O PROJETO:**

1. Instalar e configurar o JDK.
2. Instalar e configurar o Maven.
2.1. Utilizar o Maven para baixar as dependências descritas no arquivo pom.xml.
3. Instalar e configurar algum servidor web java (foi utilizado o Tomcat).
3.1. Setar o projeto no servidor.
3.2. Subir o servidor.
4. Instalar e configurar o MySQL.
4.1. Setar a url do MySQL no arquivo persistence.xml
4.2. Configurar o fusu horario no MySQL, atravez do comando: ``SET GLOBAL time_zone = '+3:00';``.
4.3. Criar o banco conforme descrita na url persistence.xml.
5. Subir o servidor.
5.1. Caso o banco não esteja populado, rodar a classe "PopularFabricante.java" (localizada em: src/com/hepta/mercado/persistence/) como "Java Application".
5.2. Rodar o projeto pelo servidor.