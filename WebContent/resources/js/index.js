const index = new Vue({
	el:"#inicio",
	created: function(){
        this.buscaProdutos();
    },
    data: {
    	idProdutoExcluir: 0,
        listaProdutos: [],
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		],
    },    
    methods:{
        buscaProdutos: function(){
			axios.get("/mercado/rs/produtos")
			.then(response => {
				this.listaProdutos = response.data;
			}).catch(function (error) {
				this.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			});
		},
		editarProduto: function(produto) {
			localStorage.setItem("dadosProdutoEdicao", JSON.stringify(produto));
			window.location.href = "/mercado/editar-produto.html";
		},
		excluirProduto: function() {
			axios.delete("/mercado/rs/produtos/" + this.idProdutoExcluir)
			.then(response => {
				console.log('ok! \o/');
				location.reload();
			}).catch(function (error) {
				console.log('erro! =(');
			});
		},
		gravarIdProduto: function(idProduto) {
			this.idProdutoExcluir = idProduto
		}
    }
});