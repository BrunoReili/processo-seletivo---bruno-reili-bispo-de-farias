const novoProduto = new Vue({
	el: '#app',
	created: function(){
		this.buscaFabricantes();
		
		const dadosProdutoEdicao = JSON.parse(localStorage.getItem("dadosProdutoEdicao"));
		localStorage.clear();
		
		if(dadosProdutoEdicao) {	
			this.montarFormularioEdicao(dadosProdutoEdicao);
		}
    },data: {
		tituloCadastro: 'Cadastro de Novos Produtos',
		tituloEdicao: 'Edição de Produto',
		
		listafabricantes: [],
		
		mensagemSucesso: 'Produto cadastrado com sucesso!',
		mensagemErro: ' Não foi possível cadastrar o produto!',
		mensagemErroFabricante: ' O campo fabricante é obrigatório!',
		
		apresentaMensagemSucesso: false,
		apresentaMensagemErro: false,
		apresentaMensagemErroFabricante: false,
		
		idProduto: '',
		idFabricante: '',
		nome: '',
		unidade: '',
		volume: '',
		estoque: ''
	},
    methods:{
    	buscaFabricantes: function(){
			axios.get("/mercado/rs/fabricantes")
			.then(response => {
				this.listafabricantes = response.data;
			}).catch(function (error) {
				this.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			});
		},
		selecionarFabricante: function(idFabricante) {
			this.idFabricante = idFabricante;
		},
		cadastrarNovoProduto: function() {
			const produtoCadastrado = this.montarDadosParaEnvio();	
			
			if(!produtoCadastrado.fabricante.id) {
				this.apresentarMensagemErroFabricante();
				return;
			}
			
			axios.post("/mercado/rs/produtos", produtoCadastrado)
			.then(response => {
				this.apresentarMensagemSucesso();
			}).catch(function (error) {
				this.apresentarMensagemErro();
			});
		},
		editarProduto: function() {
			let produtoEditado = this.montarDadosParaEnvio();
			produtoEditado.id = this.idProduto;
			
			axios.put("/mercado/rs/produtos/" + this.idProduto, produtoEditado)
			.then(response => {
				this.apresentarMensagemSucesso();
			}).catch(function (error) {
				this.apresentarMensagemErro();
			})
		},
		montarFormularioEdicao: function(dadosProdutoEdicao) {	
			this.idProduto = dadosProdutoEdicao.id;
			this.nome = dadosProdutoEdicao.nome;
			this.idFabricante = dadosProdutoEdicao.fabricante.id;
			this.unidade = dadosProdutoEdicao.unidade;
			this.volume = dadosProdutoEdicao.volume;
			this.estoque = dadosProdutoEdicao.estoque;
			
		},
		montarDadosParaEnvio: function() {
			const novoProduto = {
					nome: this.nome,
					fabricante: {id: this.idFabricante},
					unidade: this.unidade,
					volume: this.volume,
					estoque: this.estoque
			};
			
			return novoProduto;
		},
		apresentarMensagemSucesso: function() {
			this.apresentaMensagemSucesso = true;
			setTimeout(function(){ 
				this.apresentaMensagemSucesso = false;	 
				window.location.href = "/mercado/";
			}, 1000);
		},
		apresentarMensagemErro: function() {
			this.apresentaMensagemErro = true;
			setTimeout(function(){ 
				this.apresentaMensagemErro = false;
				location.reload();
			}, 1000);
		},
		apresentarMensagemErroFabricante: function() {
			this.apresentaMensagemErroFabricante = true;
			setTimeout(function(){ 
				this.apresentaMensagemErroFabricante = false;
				location.reload();
			}, 1000);
		}
	}
});