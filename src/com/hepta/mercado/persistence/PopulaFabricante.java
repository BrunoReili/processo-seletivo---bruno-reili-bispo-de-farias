package com.hepta.mercado.persistence;

import javax.persistence.EntityManager;

import com.hepta.mercado.entity.Fabricante;

public class PopulaFabricante {
	public static void main(String[] args) {
		EntityManager em = new HibernateUtil().getEntityManager();
		
		em.getTransaction().begin();
		
		Fabricante fabricante1 = new Fabricante();
		Fabricante fabricante2 = new Fabricante();
		Fabricante fabricante3 = new Fabricante();
		
		fabricante1.setNome("Danone");
		fabricante2.setNome("Nestl�");
		fabricante3.setNome("Unilever");
		
		em.persist(fabricante1);
		em.persist(fabricante2);
		em.persist(fabricante3);
		
		em.getTransaction().commit();
		
		em.close();
	}
}
